package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 1/06/17.
 */
public class administrador {
    private JPanel panel3;
    private JTextField NomUsuari;
    private JTextField Nom;
    private JTextField Cognoms;
    private JTextField DNI;
    private JTextField Domicili;
    private JTextField Poblacio;
    private JTextField DataNaixement;
    private JPasswordField Contraseña1;
    private JButton CREARADMINISTRADORButton;
    private JPasswordField Contraseña2;

    public administrador(DataBase db){
        CREARADMINISTRADORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String contraseña1 = "";
                String contraseña2="";

                for (int i = 0; i < Contraseña1.getPassword().length; i++) {
                    contraseña1+=Contraseña1.getPassword()[i];
                }

                for (int i = 0; i < Contraseña2.getPassword().length; i++) {
                    contraseña2+=Contraseña2.getPassword()[i];
                }
                if (contraseña1.equals(contraseña2)){
                    try{
                        db.insertAdministrador(NomUsuari.getText(),contraseña1,Nom.getText().toUpperCase(),Cognoms.getText().toUpperCase(),
                                DNI.getText().toUpperCase(),Domicili.getText().toUpperCase(),Poblacio.getText().toUpperCase(),DataNaixement.getText());
                        showMessageDialog(null, "Administrador creado con exito");
                    }catch (Exception e1){
                        e1.printStackTrace();
                    }
                }else {
                    showMessageDialog(null,"Las contraseñas no coinciden");
                }
            }
        });
    }

    public JPanel getpanel(){
        return panel3;
    }
}
