package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 9/06/17.
 */
public class genero {
    private JPanel panel1;
    private JTextField genero;
    private JButton NUEVOGENEROButton;

    public genero(DataBase db){
        NUEVOGENEROButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.insertGenero(genero.getText().toUpperCase());
                    showMessageDialog(null,"Genero insertado");
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
