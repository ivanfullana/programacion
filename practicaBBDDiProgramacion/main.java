package practicaBBDDiProgramacion;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 23/05/17.
 */
public class main {
    static practicaBBDDiProgramacion.DataBase db = new practicaBBDDiProgramacion.DataBase();
    public static void main(String[] args) throws Exception {

        //String fname = "/tmp/prova_xml.xml";
        //createXML(fname);

        //Sanciones autonmaticas
        DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        int sancionesInsertadas = 0;
        try {
            sancionesInsertadas= db.compruebaPrestamos(date,db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //inicio del frame
        JFrame inicio = new JFrame();

        //barra de menu
        JMenuBar menubar = new JMenuBar();
        menubar.setVisible(false);

        //inici del cardlayout y el container
        CardLayout cards = new CardLayout();
        Container contenedor = inicio.getContentPane();

        //layouts
        login login = new login(cards,contenedor,menubar,db);
        principal principal = new principal(menubar,cards,contenedor,sancionesInsertadas);
        administrador administrador = new administrador(db);
        usuari usuari = new usuari(db);
        prestec prestec = new prestec(db);

        sancion sancion = new sancion(db);
        devolucion devolucion = new devolucion(db);
        autor autor = new autor(db);
        genero genero = new genero(db);

        deleteAdministrador deleteadministrador1 = new deleteAdministrador(db);
        deletelibro deletelibro1 = new deletelibro(db);
        deleteSancion deleteSancion1 = new deleteSancion(db);
        deleteUsuario deleteUsuario1 = new deleteUsuario(db);
        deleteAutor deleteAutor1 = new deleteAutor(db);
        deleteGenero deleteGenero = new deleteGenero(db);

        searchUsuari searchUsuari = new searchUsuari(db,contenedor,cards);
        searchLibro searchLibro = new searchLibro(db,contenedor,cards);
        searchPrestamo searchPrestamo = new searchPrestamo(db,contenedor,cards);
        searchAutor searchAutor = new searchAutor(db,cards,contenedor);
        searchSancion searchSancion = new searchSancion(db,cards,contenedor);

        editLibro editLibro = new editLibro(db);
        editPrestamo editPrestamo = new editPrestamo(db);
        editAutores editAutores = new editAutores(db);
        editUsuarios editUsuarios = new editUsuarios(db);
        editAdministrador editAdministrador = new editAdministrador(db);

        //afegim els panels al cardlayout amb el identificador

        cards.addLayoutComponent(principal.getpanel(),"Principal");
        cards.addLayoutComponent(login.getpanel(),"Login");
        cards.addLayoutComponent(administrador.getpanel(),"Administrador");
        cards.addLayoutComponent(usuari.getPanel(),"Usuari");
        cards.addLayoutComponent(prestec.getpanel(),"Prestec");
        cards.addLayoutComponent(sancion.getPanel(),"Sancion");
        cards.addLayoutComponent(searchUsuari.getPanel(),"SearchUsuario");
        cards.addLayoutComponent(searchLibro.getPanel(),"SearchLibro");
        cards.addLayoutComponent(searchPrestamo.getPanel(),"SearchPrestamo");
        cards.addLayoutComponent(searchAutor.getPanel(),"SearchAutor");
        cards.addLayoutComponent(devolucion.getPanel(),"Devolucion");
        cards.addLayoutComponent(deleteadministrador1.getPanel(),"DeleteAdministrador");
        cards.addLayoutComponent(deletelibro1.getPanel(),"DeleteLibro");
        cards.addLayoutComponent(deleteSancion1.getPanel(),"DeleteSancion");
        cards.addLayoutComponent(deleteUsuario1.getPanel(),"DeleteUsuario");
        cards.addLayoutComponent(deleteAutor1.getPanel(),"DeleteAutor");
        cards.addLayoutComponent(deleteGenero.getPanel(),"DeleteGenero");
        cards.addLayoutComponent(autor.getPanel(),"Autor");
        cards.addLayoutComponent(genero.getPanel(),"Genero");
        cards.addLayoutComponent(searchSancion.getPanel(),"SearchSancion");
        cards.addLayoutComponent(editLibro.getPanel(),"EditLibro");
        cards.addLayoutComponent(editPrestamo.getPanel(),"EditPrestamo");
        cards.addLayoutComponent(editAutores.getPanel(),"EditAutor");
        cards.addLayoutComponent(editUsuarios.getPanel(),"EditUsuario");
        cards.addLayoutComponent(editAdministrador.getPanel(),"EditAdministrador");

        //afegimn els panels al Containner

        contenedor.add(principal.getpanel());
        contenedor.add(login.getpanel());
        contenedor.add(administrador.getpanel());
        contenedor.add(prestec.getpanel());
        contenedor.add(usuari.getPanel());
        contenedor.add(sancion.getPanel());
        contenedor.add(searchUsuari.getPanel());
        contenedor.add(searchLibro.getPanel());
        contenedor.add(searchPrestamo.getPanel());
        contenedor.add(devolucion.getPanel());
        contenedor.add(deleteadministrador1.getPanel());
        contenedor.add(deletelibro1.getPanel());
        contenedor.add(deleteSancion1.getPanel());
        contenedor.add(deleteUsuario1.getPanel());
        contenedor.add(deleteGenero.getPanel());
        contenedor.add(deleteAutor1.getPanel());
        contenedor.add(autor.getPanel());
        contenedor.add(searchAutor.getPanel());
        contenedor.add(genero.getPanel());
        contenedor.add(searchSancion.getPanel());
        contenedor.add(editLibro.getPanel());
        contenedor.add(editPrestamo.getPanel());
        contenedor.add(editUsuarios.getPanel());
        contenedor.add(editAutores.getPanel());
        contenedor.add(editAdministrador.getPanel());


        contenedor.setLayout(cards);//li posam el layout de tipus cardlayout amb els que tenim ja creats
        cards.show(contenedor,"Login");//començam amb el layout anomenat login

        //Jmenu
        JMenu Nuevo = new JMenu("Nuevo");
        //Jmenuitems para el menu "Nuevo"
        JMenuItem Autor = new JMenuItem("Autor");
        Autor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Autor");
            }
        });

        JMenuItem Libro = new JMenuItem("Libro");
        Libro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                llibre llibre = new llibre(db);
                cards.addLayoutComponent(llibre.getPanel(),"Llibre");
                contenedor.add(llibre.getPanel());
                cards.show(contenedor,"Llibre");
            }
        });

        JMenuItem Presetec = new JMenuItem("Prestamo");

        Presetec.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Prestec");
            }
        });

        JMenuItem Usuario = new JMenuItem("Usuario");
        Usuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Usuari");
            }
        });

        JMenuItem Administrador = new JMenuItem("Administrador");
        Administrador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Administrador");
            }
        });

        JMenuItem Sancion = new JMenuItem("Sanción");
        Sancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Sancion");
            }
        });

        JMenuItem Devolucion = new JMenuItem("Devolucion");
        Devolucion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Devolucion");
            }
        });

        JMenuItem Genero = new JMenuItem("Genero");
        Genero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Genero");
            }
        });



        //Jmenu "Buscar
        JMenu Buscar = new JMenu("Buscar");

        //Jmenuitens para el menu "Buscar"
        JMenuItem UsuarioS = new JMenuItem("Usuario");
        UsuarioS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"SearchUsuario");
            }
        });
        JMenuItem LibroS = new JMenuItem("Libro");
        LibroS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"SearchLibro");
            }
        });
        JMenuItem PrestamoS = new JMenuItem("Prestamo");
        PrestamoS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"SearchPrestamo");
            }
        });

        JMenuItem AutorS = new JMenuItem("Autor");
        AutorS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    cards.show(contenedor,"SearchAutor");
            }
        });

        JMenuItem SancionS = new JMenuItem("Sancion");
        SancionS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"SearchSancion");
            }
        });

        //Jmenu eliminar

        JMenu eliminar = new JMenu("Eliminar");

        //Jmenuitems del menu eliminar

        JMenuItem deleteUsuario = new JMenuItem("Usuario");
        deleteUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeleteUsuario");
            }
        });
        JMenuItem deletelibro = new JMenuItem("Libro");
        deletelibro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeleteLibro");
            }
        });

        JMenuItem deleteSancion = new JMenuItem("Sancion");
        deleteSancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeleteSancion");
            }
        });
        JMenuItem deleteAdministrador = new JMenuItem("Administrador");
        deleteAdministrador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeleteAdministrador");
            }
        });

        JMenuItem deleteAutor = new JMenuItem("Autor");
        deleteAutor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeleteAutor");
            }
        });

        JMenuItem deleteGenero1 = new JMenuItem("Genero");
        deleteGenero1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"DeketeGenero");
            }
        });

        //jmenu editar
        JMenu Editar = new JMenu("Editar");

        //JmenuItems Editar
        JMenuItem EditLibro = new JMenuItem("Libro");
        EditLibro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"EditLibro");
            }
        });
        JMenuItem EditAdministrador = new JMenuItem("Administrador");
        EditAdministrador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"EditAdministrador");
            }
        });
        JMenuItem EditPrestec = new JMenuItem("Prestamo");
        EditPrestec.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"EditPrestamo");
            }
        });
        JMenuItem EditUsuario = new JMenuItem("Usuario");
        EditUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"EditUsuario");
            }
        });

        JMenuItem EditAutor = new JMenuItem("Autor");
        EditAutor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"EditAutor");
            }
        });

        //jmenu ir administrador..
        JMenu ir = new JMenu("Ir a..");

        //menu items de ir administrador..
        JMenuItem Inicio = new JMenuItem("Inicio");
        Inicio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.show(contenedor,"Principal");
            }
        });



        //afegim els menuitems administrador la barra
        Nuevo.add(Presetec);
        Nuevo.add(Usuario);
        Nuevo.add(Libro);
        Nuevo.add(Administrador);
        Nuevo.add(Sancion);
        Nuevo.add(Devolucion);
        Nuevo.add(Autor);
        Nuevo.add(Genero);

        Buscar.add(UsuarioS);
        Buscar.add(LibroS);
        Buscar.add(PrestamoS);
        Buscar.add(AutorS);
        Buscar.add(SancionS);

        ir.add(Inicio);

        eliminar.add(deleteAdministrador);
        eliminar.add(deletelibro);
        eliminar.add(deleteSancion);
        eliminar.add(deleteUsuario);
        eliminar.add(deleteAutor);
        eliminar.add(deleteGenero1);

        Editar.add(EditPrestec);
        Editar.add(EditLibro);
        Editar.add(EditUsuario);
        Editar.add(EditAutor);
        Editar.add(EditAdministrador);

        menubar.add(Nuevo);
        menubar.add(Buscar);
        menubar.add(eliminar);
        menubar.add(Editar);
        menubar.add(ir);


        //añadir la barra de menu
        inicio.setJMenuBar(menubar);

        //propiedades del frame principales
        inicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        inicio.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    db.conexion.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        inicio.setSize(500, 500);
        inicio.setVisible(true);
    }

    static void createXML(String filename) throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.newDocument();

        Element root = doc.createElement("Cars");
        doc.appendChild(root);

        Element car = doc.createElement("Car1");
        car.setAttribute("Maker", "Renault");
        car.setAttribute("Color", "White");
        root.appendChild(car);

        car = doc.createElement("Car2");
        car.setAttribute("Maker", "Mercedes");
        car.setAttribute("Color", "Red");
        root.appendChild(car);

        Transformer tf = TransformerFactory.newInstance().newTransformer();
        StreamResult sr = new StreamResult(new File(filename));
        tf.transform(new DOMSource(doc), sr);
    }

}

