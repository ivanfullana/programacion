package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DatabaseMetaData;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 2/06/17.
 */
public class sancion {
    private JPanel panel1;
    private JTextField DNI;
    private JTextField Cod_Libro;
    private JComboBox Tipo;
    private JTextArea Observaciones;
    private JButton NUEVASANCIONButton;
    private JTextField Nom_Usuario;

    public sancion(DataBase db){

        String[] tipos ={"Lleu","Greu","Suspensió"};
        Tipo.setModel(new DefaultComboBoxModel(tipos));
        NUEVASANCIONButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String tipodesancion = (String) Tipo.getSelectedItem();
                    db.insertSancion(DNI.getText().toUpperCase(),Nom_Usuario.getText(),
                            Cod_Libro.getText(),tipodesancion,Observaciones.getText().toUpperCase());
                    showMessageDialog(null,"Sancion Insertada");
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel(){
        return panel1;
    }
}
