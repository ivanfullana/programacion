package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 3/06/17.
 */
public class resultadoSeachPrestamo {
    private JPanel panel1;
    private JTable table1;


    public resultadoSeachPrestamo(ResultSet rs){
        DefaultTableModel dfm = new DefaultTableModel();
        table1.setModel(dfm);
        dfm.setColumnIdentifiers(new Object[]{"Num_Prestec","Data_Inici","Data_Fi","Codi_Llibre","DNI"});
        try {
            while (rs.next()){
                dfm.addRow(new Object[]{rs.getInt("Num_Prestec"),rs.getDate("Data_Inici"),rs.getDate("Data_Fi"),
                        rs.getInt("Codi_Llibre"),rs.getString("DNI")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public JPanel getPanel() {
        return panel1;
    }
}
