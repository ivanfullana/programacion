package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 2/06/17.
 */
public class llibre {
    private JPanel panel1;
    private JTextField Titulo;
    private JTextField Editorial;
    private JTextField NumPaginas;
    private JTextField ISBN;
    private JTextField Edicion;
    private JButton nuevoLibroButton;
    private JComboBox Cantidad;
    private JComboBox Autor;
    private JComboBox genero;


    public llibre(DataBase db){
        String[] autors = db.getAutors();
        Autor.setModel(new DefaultComboBoxModel(autors));

        String[] generos = db.getGeneros();
        genero.setModel(new DefaultComboBoxModel(generos));
        nuevoLibroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String autores = (String) Autor.getSelectedItem();
                    String Genero = (String) genero.getSelectedItem();
                    db.insertLlibre(Titulo.getText().toUpperCase(),Editorial.getText().toUpperCase(),autores.toUpperCase(),
                            Genero.toUpperCase(),NumPaginas.getText().toUpperCase(),ISBN.getText().toUpperCase(),Edicion.getText().toUpperCase(),Cantidad.getSelectedIndex());
                    showMessageDialog(null, "Libro Insertado");
                }catch (Exception e1){
                    showMessageDialog(null,"Revisa la informacion del libro, recuerda poner el titulo todo en mayúsculas y la edicion solo el numero");
                }

            }
        });
    }

    public JPanel getPanel(){
        return panel1;
    }
}