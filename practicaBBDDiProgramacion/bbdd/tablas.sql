create table Usuari(
Nom varchar(200),
Cognoms varchar(200),
Data_Naixement date,
DNI varchar(9) not null primary key,
Població varchar(200),
Domicili varchar(200),
Telefono int(9)
);

create table Llibre(
Codi_Llibre int auto_increment not null primary key,
Titol varchar(200),
Editorial varchar(200),
Autor varchar(200),
Genere varchar(200),
Num_Pagines int(200),
ISBN varchar(50),
Estado enum('Disponible','Prestado'),
Edició int(200)
);

create table Administradors(
Nom_Usuari varchar(200) primary key not null,
Contraseña varchar(200) not null,
Nom varchar(200),
Cognoms varchar(200),
Data_Naixement date,
DNI varchar(9),
Població varchar(200),
Domicili varchar(200)
);

create table Prestec(
Num_Prestec int auto_increment not null,
primary key (Num_Prestec),
Data_Inici date,
Data_Fi date,
Codi_Llibre int not null,
foreign key (Codi_Llibre) references Llibre(Codi_Llibre),
DNI varchar(9) not null,
foreign key (DNI) references Usuari(DNI)
);

create table Autor(
id_Autor int auto_increment not null primary key,
Nom varchar(200),
Data_Naixement date,
Nacionalitat varchar(200)
);

create table Genere(
id_genere int auto_increment not null primary key,
tipus varchar(50)
);

create table Sancion(
id_Sancion int not null auto_increment primary key,
DNI varchar(9),
foreign key (DNI) references Usuari(DNI),
Nom_Usuari varchar(200),
foreign key (Nom_Usuari) references Administradors(Nom_Usuari),
Codi_Llibre int not null,
foreign key (Codi_Llibre) references Llibre(Codi_Llibre),
Tipus enum('Lleu','Greu','Suspensió'),
Observacions varchar(500)
);


select Contraseña from Administradors where Nom_Usuari = 'ivan';
select * from Usuari;
select * from Administradors;
select*from Llibre;
select * from Prestec;
select * from Sancion;

insert into Prestec (Data_Inici,Data_Fi,Codi_Llibre,DNI) values
('2000-02-02','2000-03-03','2','45622332e');

delete from Llibre where Titol = 'Eclipse';

insert into Llibre (Titol,Editorial,Autor,Genere,Num_Pagines,ISBN,Estado,Edició)values
('HAMBRIENTO','Planeta','Nach','Poesia','200','97855457','Disponible','2');

update Llibre set Estado = 'Disponible' where Codi_Llibre = 2;

Select Estado from Llibre where Codi_Llibre = 1;

INSERT INTO Autor (Nom,Data_Naixement,Nacionalitat) values('Tolkien','2000-02-02','Inglesa');

select Nom from Autor;

insert into Genere (tipus) values ('POESIAs');

select Codi_Llibre,Data_Fi from Prestec;

delete from Prestec;

select * from Prestec;

select Codi_Llibre,Data_Fi from Prestec;

select * from Sancion where Codi_Llibre = 1;






