package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 1/06/17.
 */
public class prestec {
    private JPanel panel4;
    private JTextField DataInici;
    private JButton button1;
    private JTextField DataFinal;
    private JTextField CodiLlibre;
    private JTextField DNI_Usuario;

    public prestec(DataBase db) {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (db.checkEstado(CodiLlibre.getText())){
                    try {
                        db.insertPrestec(DataInici.getText(),DataFinal.getText(),CodiLlibre.getText(), DNI_Usuario.getText().toUpperCase());
                        db.updateLibroPrestamo(CodiLlibre.getText());
                        showMessageDialog(null, "Prestamo realizado");
                    }catch (Exception e1){
                        showMessageDialog(null,"Error, recuerda que el usuario tiene que estar registrado y el libro tambien. Cuiadado con el formato de la fecha");
                    }
                }else showMessageDialog(null, "El libro que esta intentando prestar no esta disponible");
            }
        });
    }

    public JPanel getpanel(){
        return panel4;
    }
}
