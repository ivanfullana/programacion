package practicaBBDDiProgramacion;

import com.sun.java.browser.plugin2.DOM;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 14/06/17.
 */
public class editUsuarios {
    private JPanel panel1;
    private JTextField DNI;
    private JTextField INFO;
    private JRadioButton NOMBRERadioButton;
    private JRadioButton DNIRadioButton;
    private JRadioButton APELLIDOSRadioButton;
    private JRadioButton FECHANACIMIENTORadioButton;
    private JRadioButton POBLACIONRadioButton;
    private JRadioButton DOMICILIORadioButton;
    private JRadioButton TELEFONORadioButton;
    private JButton ACTUALIZARUSUARIOButton;

    public editUsuarios(DataBase db) {
        ButtonGroup bg = new ButtonGroup();
        bg.add(NOMBRERadioButton);
        bg.add(DNIRadioButton);
        bg.add(APELLIDOSRadioButton);
        bg.add(FECHANACIMIENTORadioButton);
        bg.add(POBLACIONRadioButton);
        bg.add(DOMICILIORadioButton);
        bg.add(TELEFONORadioButton);
        ACTUALIZARUSUARIOButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    db.editUsuario(DNI.getText(),INFO.getText(),NOMBRERadioButton,DNIRadioButton,APELLIDOSRadioButton
                            ,FECHANACIMIENTORadioButton,POBLACIONRadioButton,DOMICILIORadioButton,TELEFONORadioButton);
                    showMessageDialog(null,"Usuario Editado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
