package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 14/06/17.
 */
public class editAdministrador {
    private JPanel panel1;
    private JTextField nom_usuario;
    private JTextField contraseña;
    private JTextField info;
    private JButton ACTUALIZARADMINISTRADORButton;
    private JRadioButton nombreUsuarioRadioButton;
    private JRadioButton contraseñaRadioButton;
    private JRadioButton DNIRadioButton;
    private JRadioButton nombreRadioButton;
    private JRadioButton apellidosRadioButton;
    private JRadioButton fechaNacimientoRadioButton;
    private JRadioButton domicilioRadioButton;
    private JRadioButton poblacionRadioButton;

    public editAdministrador(DataBase db){

        ACTUALIZARADMINISTRADORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.editAdministrador(nom_usuario.getText(),contraseña.getText(),info.getText(),nombreUsuarioRadioButton,
                            contraseñaRadioButton,DNIRadioButton,nombreRadioButton,apellidosRadioButton,fechaNacimientoRadioButton,
                            domicilioRadioButton,poblacionRadioButton);
                    showMessageDialog(null,"Administrador Editado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });

    }

    public JPanel getPanel() {
        return panel1;
    }
}
