package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 8/06/17.
 */
public class resultadoSearchAutor {
    private JPanel panel1;
    private JTable table1;


    public resultadoSearchAutor(ResultSet rs){
        DefaultTableModel dfm = new DefaultTableModel();
        table1.setModel(dfm);
        dfm.setColumnIdentifiers(new Object[]{"Nom","Data Nacimiento","Nacionalidad"});
        try {
            while (rs.next()){
                dfm.addRow(new Object[]{rs.getString("Nom"),rs.getDate("Data_Naixement"),rs.getString("Nacionalitat")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public JPanel getPanel() {
        return panel1;
    }
}
