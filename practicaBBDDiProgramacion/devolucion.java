package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 3/06/17.
 */
public class devolucion {
    private JPanel panel1;
    private JTextField Cod_Llibre;
    private JButton hacerDevolucionButton;

    public devolucion(DataBase db){
        hacerDevolucionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.doDevolucion(Cod_Llibre.getText());
                    showMessageDialog(null, "Devolucion Completa");
                }catch(Exception ex){
                    showMessageDialog(null,"Error, revisa los prestamos en Buscar>Prestamos");
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
