package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 14/06/17.
 */
public class deleteGenero {
    private JPanel panel1;
    private JTextField textField1;
    private JButton ELIMINARGENEROButton;

    public deleteGenero(DataBase db) {
        ELIMINARGENEROButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    db.deleteGenero(textField1.getText());
                    showMessageDialog(null,"Genero eliminado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
