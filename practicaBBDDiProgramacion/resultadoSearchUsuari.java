package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 3/06/17.
 */
public class resultadoSearchUsuari {
    private JPanel panel1;
    private JTable table1;

    public resultadoSearchUsuari(ResultSet rs){
        DefaultTableModel dfm = new DefaultTableModel();
        table1.setModel(dfm);
        dfm.setColumnIdentifiers(new Object[]{"Nom","Cognoms","Data_Naixement","DNI","Població","Domicili","Telefono"});
        try {
            while (rs.next()){
                dfm.addRow(new Object[]{rs.getString("Nom"),rs.getString("Cognoms"),
                        rs.getDate("Data_Naixement"),rs.getString("DNI"),rs.getString("Població"),rs.getString("Domicili"),rs.getInt("Telefono")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public JPanel getPanel() {
        return panel1;
    }
}
