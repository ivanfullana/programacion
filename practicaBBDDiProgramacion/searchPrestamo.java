package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

/**
 * Created by ivan on 3/06/17.
 */
public class searchPrestamo {
    private JPanel panel1;
    private JTextField Prestamo;
    private JButton buscarPrestamoButton;


    public searchPrestamo(DataBase db, Container contenedor, CardLayout cards) {
        buscarPrestamoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultSet rs = db.selectPrestamo(Prestamo.getText().toUpperCase());
                resultadoSeachPrestamo rsp = new resultadoSeachPrestamo(rs);
                cards.addLayoutComponent(rsp.getPanel(),"ResultadoSearchPrestamo");
                contenedor.add(rsp.getPanel());
                cards.show(contenedor,"ResultadoSearchPrestamo");
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
