package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 6/06/17.
 */
public class deleteSancion {
    private JPanel panel1;
    private JTextField textField1;
    private JButton ELIMINARSANCIONButton;

    public deleteSancion(DataBase db) {

        ELIMINARSANCIONButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.deleteSancion(textField1.getText());
                    showMessageDialog(null,"Sancion eliminada");
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        });
    }


    public JPanel getPanel() {
        return panel1;
    }
}
