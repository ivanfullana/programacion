package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 1/06/17.
 */
public class usuari {
    private JPanel panel2;
    private JTextField Nom;
    private JTextField Cognom;
    private JTextField DataNaixement;
    private JTextField DNI;
    private JTextField Poblacio;
    private JTextField Domicili;
    private JButton CREARNUEVOUSUARIOButton;
    private JTextField Telefono;

    public usuari(DataBase db){
        CREARNUEVOUSUARIOButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.insertUsuari(Nom.getText().toUpperCase(), Cognom.getText().toUpperCase()
                            , DataNaixement.getText(), DNI.getText().toUpperCase(), Poblacio.getText().toUpperCase(),
                            Domicili.getText().toUpperCase(),Telefono.getText());
                    showMessageDialog(null, "Usuario Creado con exito");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                }
            }
        });
    }

    public JPanel getPanel(){
        return panel2;
    }
}
