package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 14/06/17.
 */
public class deleteAutor {
    private JPanel panel1;
    private JTextField textField1;
    private JButton ELIMINARAUTORButton;

    public deleteAutor(DataBase db) {
        ELIMINARAUTORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    db.deleteAutor(textField1.getText());
                    showMessageDialog(null,"Autor eliminado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });
    }
    public JPanel getPanel() {
        return panel1;
    }


}
