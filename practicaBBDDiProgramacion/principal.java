package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ivan on 1/06/17.
 */
public class principal {
    private JPanel panel1;
    private JButton cerrarSesionButton;
    private JLabel Sanciones;


    public principal(JMenuBar menubar, CardLayout cards, Container contenedor,int sanciones) {
        Sanciones.setText("Se han insertado:"+sanciones+" sanciones");
        cerrarSesionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menubar.setVisible(false);
                cards.show(contenedor,"Login");
            }
        });
    }

    public JPanel getpanel() {
        return panel1;
    }
}
