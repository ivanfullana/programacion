package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 6/06/17.
 */
public class deleteUsuario {
    private JPanel panel1;
    private JTextField DNI;
    private JButton ELIMINARUSUARIOButton;

    public deleteUsuario(DataBase db) {

        ELIMINARUSUARIOButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    db.deleteUsuario(DNI.getText().toUpperCase());
                    showMessageDialog(null,"Usuario Eliminado");
                }catch (Exception e1){
                    showMessageDialog(null,"El usuario tiene un prestamo en proceso");
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }}
