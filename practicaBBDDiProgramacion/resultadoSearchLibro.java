package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 3/06/17.
 */
public class resultadoSearchLibro {
    private JPanel panel1;
    private JTable table1;

    public resultadoSearchLibro(ResultSet rs){
        DefaultTableModel dfm = new DefaultTableModel();
        table1.setModel(dfm);
        dfm.setColumnIdentifiers(new Object[]{"Codi Libre","Titol","Editorial","Autor","Genere","Num Pagines","ISBN","Estado","Edición"});
        try {
            while (rs.next()){
                dfm.addRow(new Object[]{rs.getInt("Codi_Llibre"),rs.getString("Titol"),rs.getString("Editorial"),
                        rs.getString("Autor"),rs.getString("Genere"),rs.getInt("Num_Pagines"),
                        rs.getString("ISBN"),rs.getString("Estado"),rs.getString("Edició")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public JPanel getPanel() {
        return panel1;
    }
}
