package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 3/06/17.
 */
public class searchLibro {
    private JPanel panel1;
    private JTextField info;
    private JButton BUSCARButton;
    private JRadioButton TITULORadioButton;
    private JRadioButton GENERORadioButton;
    private JRadioButton EDITORIALRadioButton;
    private JRadioButton AUTORRadioButton;
    private JRadioButton TODOSRadioButton;


    public searchLibro(DataBase db, Container contenedor, CardLayout cards){
        ButtonGroup bg = new ButtonGroup();

        bg.add(TITULORadioButton);
        bg.add(GENERORadioButton);
        bg.add(EDITORIALRadioButton);
        bg.add(AUTORRadioButton);
        bg.add(TODOSRadioButton);
        BUSCARButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    ResultSet rs = db.selectLibro(info.getText().toUpperCase(),TITULORadioButton,
                            GENERORadioButton,EDITORIALRadioButton,AUTORRadioButton,TODOSRadioButton);
                    resultadoSearchLibro rsl = new resultadoSearchLibro(rs);
                    cards.addLayoutComponent(rsl.getPanel(),"ResultadoSearchLibro");
                    contenedor.add(rsl.getPanel());
                    cards.show(contenedor,"ResultadoSearchLibro");
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
