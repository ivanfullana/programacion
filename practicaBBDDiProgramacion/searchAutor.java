package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 8/06/17.
 */
public class searchAutor {
    private JPanel panel1;
    private JTextField info;
    private JRadioButton nombreRadioButton;
    private JRadioButton nacionalidadRadioButton;
    private JRadioButton todosRadioButton;
    private JButton BUSCARAUTORButton;

    public searchAutor(DataBase db,CardLayout cards, Container contenedor){
        ButtonGroup bg = new ButtonGroup();

        bg.add(nombreRadioButton);
        bg.add(nacionalidadRadioButton);
        bg.add(todosRadioButton);
        BUSCARAUTORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultSet rs = null;
                try {
                    rs = db.selectAutor(info.getText().toUpperCase(),nombreRadioButton,nacionalidadRadioButton,todosRadioButton);
                } catch (SQLException s) {
                    s.printStackTrace();
                }
                resultadoSearchAutor rsa = new resultadoSearchAutor(rs);
                cards.addLayoutComponent(rsa.getPanel(),"ResultadoSearchAutor");
                contenedor.add(rsa.getPanel());
                cards.show(contenedor,"ResultadoSearchAutor");
            }

        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
