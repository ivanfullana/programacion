package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 6/06/17.
 */
public class deletelibro {
    private JPanel panel1;
    private JTextField Cod_Libro;
    private JButton ELIMINARLIBROButton;

    public deletelibro(DataBase db) {

        ELIMINARLIBROButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    db.deleteLibro(Cod_Libro.getText(),db);
                    showMessageDialog(null,"Libro Eliminado");
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
