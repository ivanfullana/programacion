package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 12/06/17.
 */
public class resultadoSearchSancion {
    private JPanel panel1;
    private JTable table1;
    private Component panel;

    public resultadoSearchSancion(ResultSet rs) {
        DefaultTableModel dfm = new DefaultTableModel();
        table1.setModel(dfm);
        dfm.setColumnIdentifiers(new Object[]{"Id_Sancion","DNI","Cod_Libro","Nombre_Usuario","Tipo","Observaciones"});
        try {
            while (rs.next()){
                dfm.addRow(new Object[]{rs.getInt("Id_Sancion"),rs.getString("DNI"),rs.getInt("Codi_Llibre"),rs.getString("Nom_Usuari"),
                        rs.getString("Tipus"),rs.getString("Observacions")});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Component getPanel() {
        return panel1;
    }
}
