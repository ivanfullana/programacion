package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 15/06/17.
 */
public class editPrestamo {
    private JPanel panel1;
    private JTextField NumPrestec;
    private JTextField info;
    private JRadioButton fechaInicioRadioButton;
    private JRadioButton DNIRadioButton;
    private JRadioButton fechaFinalRadioButton;
    private JRadioButton cod_LibroRadioButton;
    private JButton EDITARPRESTAMOButton;

    public editPrestamo(DataBase db){
        ButtonGroup bg = new ButtonGroup();
        bg.add(fechaFinalRadioButton);
        bg.add(fechaInicioRadioButton);
        bg.add(DNIRadioButton);
        bg.add(cod_LibroRadioButton);
        EDITARPRESTAMOButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.updatePrestamo(NumPrestec.getText(),info.getText(),fechaInicioRadioButton,fechaFinalRadioButton,cod_LibroRadioButton,DNIRadioButton);
                    showMessageDialog(null,"Prestamo Editado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });
    }
    public JPanel getPanel() {
        return panel1;
    }
}
