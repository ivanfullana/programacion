package practicaBBDDiProgramacion;

import javafx.scene.control.Alert;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 23/05/17.
 */
public class login {
    private JButton ENTERButton;
    private JPanel inicio;
    private JPasswordField passwordField1;
    private JTextField username;
    String usuario;
    public login(CardLayout inicio, Container contenedor, JMenuBar menubar,DataBase d) {
        ENTERButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String contraseña = "";
                String contraadmin = "";
                char[] password;

                usuario=username.getText();
                password = passwordField1.getPassword();
                for (int i = 0; i <password.length; i++) {
                    contraseña += password[i];
                }
                contraadmin = d.getcontraseña(usuario);
                if (contraadmin.equals(contraseña)){
                    inicio.show(contenedor,"Principal");
                    menubar.setVisible(true);
                }else {
                    showMessageDialog(null, "Error");
                }
            }
        });
    }

    public JPanel getpanel() {
        return inicio;
    }

}
