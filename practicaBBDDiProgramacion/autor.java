package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 8/06/17.
 */
public class autor {
    private JPanel panel1;
    private JTextField Nombre;
    private JTextField Nacimiento;
    private JTextField Nacionalidad;
    private JButton CREARAUTORButton;

    public autor(DataBase db) {

        CREARAUTORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.insertAutor(db,Nombre.getText().toUpperCase(),Nacimiento.getText(),Nacionalidad.getText().toUpperCase());
                    showMessageDialog(null,"Autor insertado");
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
