package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

/**
 * Created by ivan on 3/06/17.
 */
public class searchUsuari {
    private JPanel panel1;
    private JTextField DNI;
    private JButton BUSCARButton;

    public searchUsuari(DataBase db, Container contenedor, CardLayout cards){

        BUSCARButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultSet rs = db.selectUsuari(DNI.getText().toUpperCase());
                resultadoSearchUsuari rsu = new resultadoSearchUsuari(rs);
                cards.addLayoutComponent(rsu.getPanel(),"ResultadoSearchUsuari");
                contenedor.add(rsu.getPanel());
                cards.show(contenedor,"ResultadoSearchUsuari");
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
