package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 6/06/17.
 */
public class deleteAdministrador {
    private JPanel panel1;
    private JTextField textField1;
    private JButton ELIMINARADMINISTRADORButton;

    public deleteAdministrador(DataBase db) {
        ELIMINARADMINISTRADORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    db.deleteAdministrador(textField1.getText());
                    showMessageDialog(null,"Administrador Eliminado");
                }catch (Exception e1){
                    e1.printStackTrace();
                }

            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
