package practicaBBDDiProgramacion;

import javax.swing.*;
import java.sql.*;
import java.util.*;
import java.util.Date;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 23/05/17.
 */
public class DataBase {
    Connection conexion;


    public DataBase() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/programacion", "root", "colegas0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getcontraseña(String usuario) {
        try {
            Statement s = conexion.createStatement();
            ResultSet rs = s.executeQuery("SELECT Contraseña FROM Administradors WHERE Nom_Usuari ='" + usuario + "'");
            if (rs.next()) {
                String contraseña = rs.getString("Contraseña");
                return contraseña;
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void insertUsuari(String NomText, String CognomText, String DataNaixementText,
                             String DNIText, String PoblacioText, String DomiciliText, String Telefono) {
        try {
            Statement s = conexion.createStatement();
            s.execute("insert into Usuari (Nom,Cognoms,Data_Naixement,DNI,Població,Domicili,Telefono) values ('" +
                    NomText + "'," + "'" + CognomText + "'," + "'" + DataNaixementText + "'," + "'" + DNIText + "'," + "'" + PoblacioText + "'," + "'" + DomiciliText + "'," +
                    "'" + Telefono + "')");

        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }


    public void insertAdministrador(String NomUsuari, String Contraseña, String Nom, String Cognom,
                                    String DNI, String Domicili, String Poblacio, String DataNaixement) {
        Statement s = null;
        try {
            s = conexion.createStatement();
            s.execute("insert into Administradors (Nom_Usuari,Contraseña,Nom,Cognoms,Data_Naixement,DNI,Població,Domicili) values ('" +
                    NomUsuari + "'," + "'" + Contraseña + "'," + "'" + Nom + "'," + "'" + Cognom + "'," +
                    "'" + DataNaixement + "'," + "'" + DNI + "'," + "'" + Poblacio + "'," + "'" + Domicili + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void insertLlibre(String Titulo, String Editioral, String Autor,
                             String Genero, String NumPagines, String ISBN, String Edicion, int cantidad) {
        Statement s = null;
        try {
            for (int i = 0; i < cantidad; i++) {
                s = conexion.createStatement();
                s.execute("insert into Llibre (Titol,Editorial,Autor,Genere,Num_Pagines,ISBN,Estado,Edició)values ('" +
                        Titulo + "'," + "'" + Editioral + "'," + "'" + Autor + "'," + "'" + Genero + "'," +
                        "'" + NumPagines + "'," + "'" + ISBN + "'," + "'Disponible'," + "'" + Edicion + "')");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertPrestec(String DataIniciText, String DataFinalText, String CodiLlibre, String DNIUsuari) {
        Statement s = null;
        try {
            s = conexion.createStatement();
            s.execute("insert into Prestec (Data_Inici,Data_Fi,Codi_Llibre,DNI) values ('" +
                    DataIniciText + "'," + "'" + DataFinalText + "'," + "'" + CodiLlibre + "'," + "'" + DNIUsuari + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ResultSet selectUsuari(String DNIText) {
        Statement s = null;
        ResultSet rs = null;
        if (DNIText.equals("TODOS")) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Usuari");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {

            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Usuari where DNI = '" + DNIText + "';");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }

    public ResultSet selectLibro(String info, JRadioButton TITULO, JRadioButton GENERO, JRadioButton EDITORIAL, JRadioButton AUTOR
            , JRadioButton TODOS) {
        Statement s = null;
        ResultSet rs = null;

        if (TITULO.isSelected()) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Llibre where Titol = '" + info + "'");
                return rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (GENERO.isSelected()) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Llibre where Genere = '" + info + "'");
                return rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (AUTOR.isSelected()) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Llibre where Autor = '" + info + "'");
                return rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (EDITORIAL.isSelected()) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Llibre where Editorial = '" + info + "'");
                return rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (TODOS.isSelected()) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Llibre");
                return rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        if (true) {
            showMessageDialog(null, "Elige una opcion porfavor");
        }

        return rs;
    }

    public ResultSet selectPrestamo(String DNI_Usuario) {
        Statement s = null;
        ResultSet rs = null;
        if (DNI_Usuario.equals("TODOS")) {
            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Prestec");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {

            try {
                s = conexion.createStatement();
                rs = s.executeQuery("Select * from Prestec where DNI = '" + DNI_Usuario + "';");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }

    public void updateLibroPrestamo(String CodiLlibreText) {
        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("update Llibre set Estado = 'Prestado' where Codi_Llibre =" + CodiLlibreText);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkEstado(String CodiLlibreText) {
        Statement s = null;
        String estado = "";
        try {
            s = conexion.createStatement();
            ResultSet rs = s.executeQuery("Select Estado from Llibre where Codi_Llibre = " + CodiLlibreText);

            if (rs.next()) {
                estado = rs.getString("Estado");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (estado.equals("Disponible")) {
            return true;
        } else return false;
    }

    public void doDevolucion(String Cod_LlibreText) {
        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("update Llibre set Estado = 'Disponible' where Codi_Llibre =" + Cod_LlibreText);
            s = conexion.createStatement();
            s.execute("delete from Prestec where Codi_Llibre = " + Cod_LlibreText);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteAdministrador(String NomUsuario) {
        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("Delete from Administradors where Nom_Usuari = '" + NomUsuario + "'");
            showMessageDialog(null, "Administrador eliminado con exito");
        } catch (SQLException e) {
            e.printStackTrace();
            showMessageDialog(null, "Error");
        }
    }

    public void deleteUsuario(String DNIText) {
        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("delete * from Sancion where DNI = '"+DNIText+"'");
            s= conexion.createStatement();
            s.execute("Delete from Usuari where DNI = '" + DNIText + "'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteSancion(String Cod_Sancion) {

        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("Delete from Sancion where Id_Sancion = '" + Cod_Sancion + "'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteLibro(String Cod_LibroText, DataBase db) {
        Statement s = null;

        try {
            if (db.checkEstado(Cod_LibroText)) {
                s = conexion.createStatement();
                s.execute("Delete from Llibre where Codi_Llibre = '" + Cod_LibroText + "'");
            } else showMessageDialog(null, "El libro que intenta eliminar esta en prestamo");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertAutor(DataBase db, String Nom, String NacimientoText, String NacionalidadText) {
        Statement s = null;

        try {
            s = conexion.createStatement();
            s.execute("INSERT INTO Autor (Nom,Data_Naixement,Nacionalitat)values('"+Nom+"','"+NacimientoText+"','"+NacionalidadText+"')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet selectAutor(String info, JRadioButton nombreRadioButton, JRadioButton nacionalidadRadioButton,
                                 JRadioButton todosRadioButton) throws SQLException {

        Statement s= conexion.createStatement();
        ResultSet rs = null;

        if (todosRadioButton.isSelected()){
            rs = s.executeQuery("Select * from Autor");
        }

        if (nombreRadioButton.isSelected()){
            rs = s.executeQuery("Select * from Autor where Nom = '"+info+"'");
        }

        if (nacionalidadRadioButton.isSelected()){
            rs= s.executeQuery("Select * from Autor where Nacionalitat = '"+info+"'");
        }
        if (true) {
            showMessageDialog(null, "Elige una opcion porfavor");
        }

        return rs;
    }

    public String[] getAutors() {
        List<String> list = new ArrayList<>();

        try {
            Statement s= conexion.createStatement();
            ResultSet rs = s.executeQuery("Select Nom from Autor");
            while (rs.next()){
                list.add(rs.getString("Nom"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String[] Autors = list.toArray(new String[list.size()]);


        return Autors;
    }

    public String[] getGeneros() {
        List<String> list = new ArrayList<>();
        try {
            Statement s= conexion.createStatement();
            ResultSet rs = s.executeQuery("Select tipus from Genere");
            while (rs.next()){
                list.add(rs.getString("tipus"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String[] Generos = list.toArray(new String[list.size()]);


        return Generos;
    }

    public void insertGenero(String genero) {
        try {
            Statement s = conexion.createStatement();
            s.execute("Insert into Genere (tipus) values ('"+genero+"')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertSancion(String DNI, String Nom_Usuari, String Cod_LibroText, String tipodesancion, String ObservacionesText) {
        try {
            Statement s = conexion.createStatement();
            s.execute("insert into Sancion (DNI,Nom_Usuari,Codi_Llibre,Tipus,Observacions) values ('"+
            DNI+"','"+Nom_Usuari+"','"+Cod_LibroText+"','"+tipodesancion+"','"+ObservacionesText+"')");
            if (tipodesancion.equals("Suspensió")){
                showMessageDialog(null,"Usuario con DNI: "+DNI+"debe ser eliminado por tener una Sancionde tipo:Suspensió ");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            showMessageDialog(null,"Error");
        }
    }

    public ResultSet selectSancion(String INFOText, JRadioButton dniRadioButton, JRadioButton cod_llibreRadioButton,
                                   JRadioButton leveRadioButton, JRadioButton suspensionRadioButton, JRadioButton graveRadioButton,
                                   JRadioButton todasRadioButton) {
Statement s= null;
ResultSet rs = null;
        try {
            if (dniRadioButton.isSelected()){
                s = conexion.createStatement();
                rs = s.executeQuery("select * from Sancion where DNI = '"+INFOText+"'");
            }

            if (cod_llibreRadioButton.isSelected()){
                s = conexion.createStatement();
                rs = s.executeQuery("select * from Sancion where Codi_Llibre = '"+INFOText+"'");
            }

            if (leveRadioButton.isSelected()){
                s= conexion.createStatement();
                rs = s.executeQuery("select * from Sancion where Tipus = 'Lleu'");
            }

            if (graveRadioButton.isSelected()){
                s = conexion.createStatement();
                rs = s.executeQuery("select * from Sancion where Tipus = 'Greu'");
            }

            if (suspensionRadioButton.isSelected()){
                s = conexion.createStatement();
                rs= s.executeQuery("select * from Sancion where Tipus = 'Suspensió'");
            }

            if (todasRadioButton.isSelected()){
                s=conexion.createStatement();
                rs= s.executeQuery("select * from Sancion");
            }
            if (true) {
                showMessageDialog(null, "Elige una opcion porfavor");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            showMessageDialog(null, "Error");
        }
        return rs;
    }

    public int compruebaPrestamos(java.util.Date actualdate, DataBase db) throws SQLException {

        Statement s = conexion.createStatement();
        ResultSet rs = s.executeQuery("select Codi_Llibre,Data_Fi from Prestec");
        ResultSet rs2 = null;
        int count = 0;
        while (rs.next()){
            Date fechaprestamo = rs.getDate("Data_Fi");
            if (actualdate.after(fechaprestamo)){
                s= conexion.createStatement();
                rs2 = s.executeQuery("select * from Prestec where Codi_Llibre = "+rs.getInt("Codi_Llibre"));
                rs2.next();
                String codillibre = String.valueOf(rs2.getInt("Codi_Llibre"));
                db.insertSancion(rs2.getString("DNI"),"Automatica",codillibre,"Lleu","Arribada de un prestec tard");
                count++;
            }
        }
        return count;
    }

    public void updateLibro(String cod_libroText, String infoText, JRadioButton tituloRadioButton, JRadioButton editorialRadioButton,
                            JRadioButton numeroPaginasRadioButton, JRadioButton isbnRadioButton, JRadioButton edicionRadioButton) {

        Statement s=null;

        if (tituloRadioButton.isSelected()){
            try {
                s =conexion.createStatement();
                s.execute("update Llibre set Titol = '"+infoText+"' where Codi_Llibre = '"+cod_libroText+"'");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

         if (editorialRadioButton.isSelected()){
             try {
                 s =conexion.createStatement();
                 s.execute("update Llibre set Editorial = '"+infoText+"' where Codi_Llibre = '"+cod_libroText+"'");
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
         if (numeroPaginasRadioButton.isSelected()){
             try {
                 s =conexion.createStatement();
                 s.execute("update Llibre set Num_Pagines = '"+infoText+"' where Codi_Llibre = '"+cod_libroText+"'");
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
         if (isbnRadioButton.isSelected()){
             try {
                 s =conexion.createStatement();
                 s.execute("update Llibre set ISBN = '"+infoText+"' where Codi_Llibre = '"+cod_libroText+"'");
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
         if (edicionRadioButton.isSelected()){
             try {
                 s =conexion.createStatement();
                 s.execute("update Llibre set Edició = '"+infoText+"' where Codi_Llibre = '"+cod_libroText+"'");
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
        if (true) {
            showMessageDialog(null, "Elige una opcion porfavor");
        }
    }

    public void updatePrestamo(String num_prestamoText, String infolText, JRadioButton fechaInicioRadioButton,
                               JRadioButton fechaFinalRadioButton, JRadioButton cod_LibroRadioButton, JRadioButton DNIRadioButton) {

        Statement s = null;

        try {
            if (fechaFinalRadioButton.isSelected()){
                s= conexion.createStatement();
                s.execute("update Prestec set Data_Final = '"+infolText+"' where Num_Prestec = '"+num_prestamoText+"'");
            }

            if (fechaInicioRadioButton.isSelected()){
                s= conexion.createStatement();
                s.execute("update Prestec set Data_Inici = '"+infolText+"' where Num_Prestec = '"+num_prestamoText+"'");
            }

            if (DNIRadioButton.isSelected()){
                s= conexion.createStatement();
                s.execute("update Prestec set DNI = '"+infolText+"' where Num_Prestec = '"+num_prestamoText+"'");
            }

            if (cod_LibroRadioButton.isSelected()){
                s= conexion.createStatement();
                s.execute("update Prestec set Codi_Llibre = '"+infolText+"' where Num_Prestec = '"+num_prestamoText+"'");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteAutor(String text) {
        try {
            Statement s = conexion.createStatement();
            s.execute("delete from Autor where id_Autor = '"+text+"'");
            s=conexion.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editAutor(String id_autorText, String infoText, JRadioButton nombreRadioButton, JRadioButton nacionalidadRadioButton,
                          JRadioButton fechaDeNcimientoAAAARadioButton) {

        try{
            Statement s = conexion.createStatement();

            if (nombreRadioButton.isSelected()){
                s.execute("update Autor set Nom = '"+infoText+"' where id_Autor = '"+id_autorText+"'");
            }
            if (nacionalidadRadioButton.isSelected()){
                s.execute("update Autor set Nacionalitat = '"+infoText+"' where id_Autor = '"+id_autorText+"'");
            }
            if (fechaDeNcimientoAAAARadioButton.isSelected()){
                s.execute("update Autor set Data_Naixement = '"+infoText+"' where id_Autor = '"+id_autorText+"'");
            }
            if (true) {
                showMessageDialog(null, "Elige una opcion porfavor");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void editUsuario(String DNIText, String INFOText, JRadioButton nombreRadioButton, JRadioButton dniRadioButton,
                            JRadioButton apellidosRadioButton, JRadioButton fechanacimientoRadioButton,
                            JRadioButton poblacionRadioButton, JRadioButton domicilioRadioButton, JRadioButton telefonoRadioButton) {
            try{
                Statement s = conexion.createStatement();

                if (nombreRadioButton.isSelected()){
                    s.execute("update Usuario set Nom = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (dniRadioButton.isSelected()){
                    s.execute("update Usuario set DNI = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (apellidosRadioButton.isSelected()){
                    s.execute("update Usuario set Cognoms = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (fechanacimientoRadioButton.isSelected()){
                    s.execute("update Usuario set Data_Naixement = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (poblacionRadioButton.isSelected()){
                    s.execute("update Usuario set Població = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (domicilioRadioButton.isSelected()){
                    s.execute("update Usuario set Domicili = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }

                if (telefonoRadioButton.isSelected()){
                    s.execute("update Usuario set Telefono = '"+INFOText+"' where DNI= '"+DNIText+"'");
                }
                if (true) {
                    showMessageDialog(null, "Elige una opcion porfavor");
                }
            }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteGenero(String textField1Text) {
        try {
            Statement s = conexion.createStatement();
            s.execute("Delete from Genere where tipus = '"+textField1Text+"'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editAdministrador(String nom_usuarioText, String contraseñaText, String infoText,
                                  JRadioButton nombreUsuarioRadioButton, JRadioButton contraseñaRadioButton,
                                  JRadioButton dniRadioButton, JRadioButton nombreRadioButton,
                                  JRadioButton apellidosRadioButton, JRadioButton fechaNacimientoRadioButton,
                                  JRadioButton domicilioRadioButton, JRadioButton poblacionRadioButton) {
            Statement s = null;
            String contraseña = null;
        try {
           s = conexion.createStatement();
            ResultSet rs = s.executeQuery("Select Contraseña from Administradors where Nom_Usuari = '"+nom_usuarioText+"'");
            rs.next();
            contraseña = rs.getString("Contraseña");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (contraseñaText.equals(contraseña)){
            try {
                if (nombreUsuarioRadioButton.isSelected()){
                    s.execute("update Administradors set Nom_Usuari = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }
                if (contraseñaRadioButton.isSelected()){
                    s.execute("update Administradors set Contraseña = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (dniRadioButton.isSelected()){
                    s.execute("update Administradors set DNI = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (nombreRadioButton.isSelected()){
                    s.execute("update Administradors set Nom = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (apellidosRadioButton.isSelected()){
                    s.execute("update Administradors set Apellidos = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (fechaNacimientoRadioButton.isSelected()){
                    s.execute("update Administradors set Data_Naixement = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (domicilioRadioButton.isSelected()){
                    s.execute("update Administradors set Domicili = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

                if (poblacionRadioButton.isSelected()){
                    s.execute("update Administradors set Població = '"+infoText+"' where Nom_Usuari = '"+nom_usuarioText+"'");
                }

            }catch (Exception e1){
                e1.printStackTrace();
            }
        }
    }
}
