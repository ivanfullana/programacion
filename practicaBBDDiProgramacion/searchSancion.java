package practicaBBDDiProgramacion;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

/**
 * Created by ivan on 12/06/17.
 */
public class searchSancion {
    private JPanel panel1;
    private JTextField INFO;
    private JRadioButton DNIRadioButton;
    private JRadioButton COD_LLIBRERadioButton;
    private JRadioButton LEVERadioButton;
    private JRadioButton SUSPENSIONRadioButton;
    private JRadioButton GRAVERadioButton;
    private JRadioButton TODASRadioButton;
    private JButton BUSCARSANCIONButton;

    public searchSancion(DataBase db, CardLayout cards, Container contenedor) {
        ButtonGroup bg = new ButtonGroup();

        bg.add(DNIRadioButton);
        bg.add(COD_LLIBRERadioButton);
        bg.add(LEVERadioButton);
        bg.add(GRAVERadioButton);
        bg.add(SUSPENSIONRadioButton);
        bg.add(TODASRadioButton);

        BUSCARSANCIONButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ResultSet rs = db.selectSancion(INFO.getText().toUpperCase(),DNIRadioButton,COD_LLIBRERadioButton,LEVERadioButton,
                        SUSPENSIONRadioButton,GRAVERadioButton,TODASRadioButton);

                resultadoSearchSancion rss = new resultadoSearchSancion(rs);
                cards.addLayoutComponent(rss.getPanel(),"ResultadoSearchSancion");
                contenedor.add(rss.getPanel());
                cards.show(contenedor,"ResultadoSearchSancion");

            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
