package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 13/06/17.
 */
public class editAutores {
    private JPanel panel1;
    private JTextField id_autor;
    private JTextField info;
    private JRadioButton nombreRadioButton;
    private JRadioButton nacionalidadRadioButton;
    private JRadioButton fechaDeNcimientoAAAARadioButton;
    private JButton EDITARAUTORSButton;

    public editAutores(DataBase db) {
        ButtonGroup bg = new ButtonGroup();
        bg.add(nombreRadioButton);
        bg.add(nacionalidadRadioButton);
        bg.add(fechaDeNcimientoAAAARadioButton);
        EDITARAUTORSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.editAutor(id_autor.getText(),info.getText().toUpperCase(),nombreRadioButton,nacionalidadRadioButton,
                            fechaDeNcimientoAAAARadioButton);
                    showMessageDialog(null,"Autor Editado");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
