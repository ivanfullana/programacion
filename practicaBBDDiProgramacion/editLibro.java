package practicaBBDDiProgramacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Created by ivan on 13/06/17.
 */
public class editLibro {
    private JPanel panel1;
    private JTextField cod_libro;
    private JTextField info;
    private JRadioButton tituloRadioButton;
    private JRadioButton editorialRadioButton;
    private JRadioButton numeroPaginasRadioButton;
    private JRadioButton ISBNRadioButton;
    private JRadioButton edicionRadioButton;
    private JButton EDITARLIBROButton;


    public editLibro(DataBase db) {
        ButtonGroup bg = new ButtonGroup();
        bg.add(tituloRadioButton);
        bg.add(editorialRadioButton);
        bg.add(numeroPaginasRadioButton);
        bg.add(ISBNRadioButton);
        bg.add(edicionRadioButton);

        EDITARLIBROButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    db.updateLibro(cod_libro.getText(),info.getText().toUpperCase(),tituloRadioButton,editorialRadioButton,numeroPaginasRadioButton,
                            ISBNRadioButton,edicionRadioButton);
                    showMessageDialog(null,"Libro editado correctamente");
                }catch (Exception e1){
                    showMessageDialog(null,"Error");
                    e1.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel1;
    }
}
