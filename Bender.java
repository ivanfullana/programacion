/**
 * Created by ivan on 16/03/17.
 */

    class Bender {
    String map;
    int VerticalX = 0;//posicion inicial en cuanto al primer numero de nuestro array bidimensional
    int HorizontalX =0;//posicion inicial en cuanto al segundo numero de nuestro array bidimensional;
    boolean finalizador = true;
    char[][] caracters;
    // Constructor: ens passen el mapa en forma d'String
    public Bender(String mapa) {
        this.map = mapa;
    }

    // Navegar fins a l'objectiu («$»).
// El valor retornat pel mètode consisteix en una cadena de
// caràcters on cada lletra pot tenir els valors «S», «N», «W» o «E»,
// segons la posició del robot a cada moment.
    public String run() {
        StringBuilder pasos = new StringBuilder();
        String[] MapParts = map.split("\n");
        caracters = new char[MapParts.length][MapParts[0].length()];
        int VerticalD = 0;//posicion final en cuanto al primer numero de nuestro array bidimensional
        int HorizontalD = 0;//posicion final en cuanto al segundo numero de nuestro array bidimensional
        String[] PuntosCardinales = {"S", "E", "N", "W"};
        String[] PuntosCardinalesInversos = {"N", "W", "S", "E"};
        boolean inversor = false;


        //copiar el mapa fragmentado en el array bidimensional
        for (int i = 0; i <caracters.length ; i++) {
            for (int j = 0; j <caracters[0].length ; j++) {
                caracters[i][j] = MapParts[i].charAt(j);
            }
        }
        //buscar posicion de inicio(X) i de final ($)
        for (int i = 0; i <caracters.length ; i++) {
            for (int j = 0; j <caracters[0].length; j++) {
                if (caracters[i][j] == 'X'){
                    VerticalX = i;
                    HorizontalX = j;
                }
                if (caracters[i][j] == '$'){
                    VerticalD = i;
                    HorizontalD = j;
                }
            }
        }

        while (VerticalX != VerticalD || HorizontalD != HorizontalX) {
            if (inversor){
                if (caracters[VerticalX-1][HorizontalX] != '#'){
                    while (caracters[VerticalX-1][HorizontalX] != '#'){
                        pasos.append(PuntosCardinalesInversos[0]);
                        VerticalX--;
                        if (caracters[VerticalX][HorizontalX] == '$'){
                            break;
                        }
                        if (caracters[VerticalX][HorizontalX] == 'T'){
                            transllada();
                        }
                        finalizador = true;
                    }
                    continue;
                }


                if (caracters[VerticalX][HorizontalX-1] != '#'){
                    while (caracters[VerticalX][HorizontalX-1] != '#'){
                        pasos.append(PuntosCardinalesInversos[1]);
                        HorizontalX--;
                        if (caracters[VerticalX][HorizontalX] == '$'){
                            break;
                        }
                        if (caracters[VerticalX][HorizontalX] == 'T'){
                            transllada();
                        }
                        finalizador = true;
                    }
                    continue;
                }

                if (caracters[VerticalX+1][HorizontalX] != '#'){
                    while (caracters[VerticalX+1][HorizontalX] != '#'){
                        pasos.append(PuntosCardinalesInversos[2]);
                        VerticalX++;
                        if (caracters[VerticalX][HorizontalX] == '$'){
                            break;
                        }
                        if (caracters[VerticalX][HorizontalX] == 'T'){
                            transllada();
                        }
                        finalizador = true;
                    }
                    continue;
                }

                if (caracters[VerticalX][HorizontalX+1] != '#'){
                    while (caracters[VerticalX][HorizontalX+1] != '#'){
                        pasos.append(PuntosCardinalesInversos[3]);
                        HorizontalX++;
                        if (caracters[VerticalX][HorizontalX] == '$'){
                            break;
                        }
                        if (caracters[VerticalX][HorizontalX] == 'T'){
                            transllada();
                        }
                        finalizador = true;
                    }
                    continue;
                }

            }else

            if (caracters[VerticalX+1][HorizontalX] != '#'){
                while (caracters[VerticalX+1][HorizontalX] != '#'){
                    pasos.append(PuntosCardinales[0]);
                    VerticalX++;
                    if (caracters[VerticalX][HorizontalX] == '$'){
                        break;
                    }
                    if (caracters[VerticalX][HorizontalX] == 'T'){
                       transllada();
                    }
                    finalizador = true;
                    if(caracters[VerticalX][HorizontalX] == 'I'){
                        inversor = true;
                        break;
                    }

                }
                continue;
            }
            if (caracters[VerticalX][HorizontalX+1] != '#'){
                while (caracters[VerticalX][HorizontalX+1] != '#'){
                    pasos.append(PuntosCardinales[1]);
                    HorizontalX++;
                    if (caracters[VerticalX][HorizontalX] == '$'){
                        break;
                    }
                    if (caracters[VerticalX][HorizontalX] == 'T'){
                      transllada();
                    }
                    finalizador = true;
                    if(caracters[VerticalX][HorizontalX] == 'I'){
                        inversor = true;
                        break;
                    }
                }
                continue;
            }
            if (caracters[VerticalX-1][HorizontalX] != '#'){
                while (caracters[VerticalX-1][HorizontalX] != '#'){
                    pasos.append(PuntosCardinales[2]);
                    VerticalX--;
                    if (caracters[VerticalX][HorizontalX] == '$'){
                        break;
                    }
                    if (caracters[VerticalX][HorizontalX] == 'T'){
                        transllada();
                    }
                    finalizador = true;
                    if(caracters[VerticalX][HorizontalX] == 'I'){
                        inversor = true;
                        break;
                    }
                }
                continue;
            }
            if (caracters[VerticalX][HorizontalX-1] != '#'){
                while (caracters[VerticalX][HorizontalX-1] != '#'){
                    pasos.append(PuntosCardinales[3]);
                    HorizontalX--;
                    if (caracters[VerticalX][HorizontalX] == '$'){
                        break;
                    }
                    if (caracters[VerticalX][HorizontalX] == 'T'){
                        transllada();
                    }
                    finalizador = true;
                    if(caracters[VerticalX][HorizontalX] == 'I'){
                        inversor = true;
                        break;
                    }
                }
                continue;
            }
            }
        return String.valueOf(pasos);
    }

    private void transllada(){
        for (int i = 0; i <caracters.length ; i++) {
            for (int j = 0; j <caracters[0].length ; j++) {
                if (finalizador) {
                    if (caracters[i][j] == 'T' && (HorizontalX != j || VerticalX != i)) {
                        VerticalX = i;
                        HorizontalX = j;
                        finalizador = false;
                    }
                }
            }
        }

    }
}