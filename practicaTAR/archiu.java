package practicaTAR;

/**
 * Created by ivan on 21/05/17.
 */
class archiu {//nova clase amb el construcor del objecte archiu
    String Nom= "";//variable on guardarem el nom del archiu
    int pes = 0;//variable on guardam el pes del archiu
    byte[] bytes;//variable on guardam els bytes del archiu

    public archiu(String nom, int pes,byte[] bytes){//constructor
        this.bytes=bytes;
        this.Nom=nom;
        this.pes=pes;
    }

    public byte[] getbyte() {
        return bytes;
    }//getter per als bytes

    public String getNom() {
        return Nom;
    }//getter per al nom
}
