package practicaTAR;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by ivan on 10/05/17.
 */
public class Tar {
    String filename; //variable on guardam la ruta que ens pasen
    List<archiu> llistadearchius = new ArrayList<>();//llista de archius del .tar
    // Constructor
    public Tar(String filename) {
        this.filename = filename;//guardam el nom/ruta que ens pasen per utilitzarlo al expand
    }
    // Torna un array amb la llista de fitxers que hi ha dins el TAR
    public String[] list() {
        String[] archius = new String[llistadearchius.size()];//cream un array amb la mateixa longitud que la llista
        for (int i = 0; i < archius.length ; i++) {//miram archiu per archiu de la llista
            archius[i]= llistadearchius.get(i).getNom();//copiam tansolsel nom de cada archiu dins l nou array.
        }
        return archius;
    }
    // Torna un array de bytes amb el contingut del fitxer que té per nom
    // igual a l'String «name» que passem per paràmetre
    public byte[] getBytes(String name) {
        for (int i = 0; i < llistadearchius.size() ; i++) {//miram dins la nostra llista
            // un archiu que tengui el mateix nom que els que ens han passat
            if(name.equals(llistadearchius.get(i).getNom())){//quan trobam el archiu que coincideix el nnom, retornam els bytes
                return llistadearchius.get(i).getbyte();
            }
        }
        return null;
    }
    // Expandeix el fitxer TAR dins la memòria
    public void expand() throws IOException {
        InputStream is = new FileInputStream(this.filename);
        String nom="";
        int d ;
        String pesoctal = "";
        int pesreal ;

        while ((d = is.read())!= -1){//mentres quedin archius dins el .tar..
            //File name
            for (int i = 0; i <100 ; i++) {//miram els 100 primers bytes del header
                if (d > 0){//si es un caracter l'afegim al nom
                    nom += (char)d;
                }
                d = is.read();//llegim el següent, el primer no fa falta llegir-lo
                // perque al iniciar el while ja ho feim
            }
            // Si el nom esta bui significa que hem arribat al darrer header i per tant turam el bucle.
            if (nom.equals("")){
                break;
            }
            //botam(skip) 24 posicions per començar a llegir el pes del archiu en qüestió
            is.skip(24);
            //File size
            // Aquest for el que mira es el pes del arxiu en octal
            for (int i = 0; i <10 ; i++) {//el pes esta en els 10següents bytes per tant..
                pesoctal +=(char)is.read();//com .read retorna un String,
                //el guardarem dins un, y mes tard el pasarem a int a mes de a nombre decimal
                // ja que el pes es triba en octal.
            }
            //una vegada tenim el pes en octal, el pasam a decimal i de seguit a int
            pesreal = Integer.parseInt(pesoctal, 8);
            //feim un altre bot(skip) de 377 posicions per començar a obtenir els bytes del archiu.
            is.skip(377);
            //File bytes
            //cream un OutputStream de bytes per emmagatzemar el bytes.
            ByteArrayOutputStream reserva = new ByteArrayOutputStream();
            for (int i = 0; i < pesreal ; i++) {//com coneixem el pes sabe cuantes vegades hem de mirar, per tant.
                reserva.write(is.read());
            }
            //ara tan sols hem de convertir el OutputStream a un array de bytes
            byte [] bytes = reserva.toByteArray();
            //per si el archiu no ocupa els 512 bytes,
            // hem de calcular que ens queda fins arribar al següent header i fer un skip.
            int falta = 512 -(pesreal % 512);
            is.skip(falta);
            //per emmagatzemar les tres dades del archiu,
            //crearem un nou objecte que accepti com a parametres el no, el pesreal, i els bytes
            llistadearchius.add(new archiu(nom,pesreal,bytes));
            //biudam les varieables del pes i el nom ja que feim += i no sustituiriem el que hi ha dedins
            nom = "";
            pesoctal = "";
        }

    }
}

