package practicaTAR;

import com.sun.xml.internal.messaging.saaj.packaging.mime.util.LineInputStream;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;



/**
 * Created by ivan on 21/05/17.
 */
public class TarMain {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        System.out.println("Bennvingut al descompressor d'archius TAR de Ivan Caballero");
        Scanner s = new Scanner(System.in);
        System.out.println("Per favor, especifica la ruta del archiu TAR en questió");
        String ruta = s.nextLine();
        File f = new File (ruta);
        boolean comprovador = true;

        while (comprovador){
            if (!f.isFile() || !ruta.contains(".tar")){
                System.out.println("La ruta esta malament, o el archiu no es correcte");
                System.out.println("Per favor, especifica la ruta del archiu TAR en questió");
                ruta = s.nextLine();
                f = new File (ruta);
                continue;
            }else{
                comprovador = false;
            }

            Tar tar = new Tar(ruta);
            tar.expand();
            System.out.println("L'archiu es correcte");
            boolean continua = true;
            while (continua){
                System.out.println("Quina acció desitges fer: LIST, BYTES, HELP, EXIT");
                String opcio = s.nextLine();

                switch (opcio){
                    case "LIST":
                        String[] archius = tar.list();
                        for (int i = 0; i <archius.length ; i++) {
                            System.out.println(archius[i]);
                        }
                        break;
                    case "BYTES":
                        System.out.println("Per aquesta opció hauras de proporcionar el nom del archiu, " +
                                "si no el saps, pots utilitzar l'opció LIST.");
                        System.out.println("Recorda posar la extensó també(.png per exemple)");
                        String archiu = s.nextLine();
                        byte[] data = tar.getBytes(archiu);

                        if (data!=null){
                            MessageDigest md = MessageDigest.getInstance("MD5");
                            md.update(data);
                            System.out.println("Els bytes del archiu son: " + toHex(md.digest()));
                        }else{
                            System.out.println("El archiu que has escrit no existeix");
                        }
                        break;
                    case "HELP":
                        System.out.println("Aqui trobaras l'informació de que fa cada mètode");
                        System.out.println("-LIST: Fa una llista de tots els archius que es troben dins el tar");
                        System.out.println("-BYTES: Per aquesta opció necesitaras saber el nom del archiu " +
                                "i la seva extensió aií que es recomenable fer un list abans si no ho saps. " +
                                "Aquesta opció retorna els bytes del archiu escrit. ");
                        System.out.println("-EXIT: Surt del programa per complet.");
                        break;
                    case "EXIT":
                        continua = false;
                        break;
                    default:
                        System.out.println("Això no es una opció, " +
                                "recorda que la has de escriure en majuscules i tal com surt a la llista");
                        break;
                }
            }
        }
    }

    private static String toHex(byte[] bytes) {
        BigInteger bi = new BigInteger(1, bytes);
        return String.format("%0" + (bytes.length << 1) + "X", bi);
    }
}
