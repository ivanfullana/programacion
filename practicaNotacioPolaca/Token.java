package practicaNotacioPolaca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Token {
    enum Toktype {//tipus de caracters que trobarem a la expressió
        NUMBER, OP, PAREN
    }

    // Pensa a implementar els "getters" d'aquests atributs
    private Toktype ttype;//variable tipusn del caracter
    private int value;// si es un numero aqui guardarem el seu valor
    private char tk;//si es un parentesis o operador el guardarem aqui

    public int getValue() {return value;}//getter per obtenir el valor de un numero

    public char getTk(){
        return tk;
    }//getter per obtenir el tk(operador o parentesis).

    public Toktype getTtype() {
        return ttype;
    }//getter per obtenir el tipus del token

    // Constructor privat. Evita que es puguin construir objectes Token externament
    private Token() {
    }

    // Torna un token de tipus "NUMBER"
    static Token tokNumber(int value) {//crea un token numeric amb el ttype NUMBER i el value sera el numero en questió
        Token t = new Token();
        t.ttype=Toktype.NUMBER;
        t.value = value;
        return t;
    }

    // Torna un token de tipus "OP"
    static Token tokOp(char c) {//crea un token de operador amb el ttype OP i el tk sera el operador en questió
        Token t = new Token();
        t.ttype= Toktype.OP;
        t.tk = c;
        return t;
    }

    // Torna un token de tipus "PAREN"
    static Token tokParen(char c) {//crea un token de parentesis amb el ttype PAREN i el tk sera el parentesis en questió
        Token t = new Token();
        t.ttype= Toktype.PAREN;
        t.tk = c;
        return t;
    }

    // Mostra un token (conversió a String)
    public String toString() {//imprimeix un token
        if (this.ttype == Toktype.NUMBER) { return "" + this.value; }
        if (this.ttype == Toktype.OP) { return "" + this.tk; }
        if (this.ttype == Toktype.PAREN) { return "" + this.tk; }
        return "";
    }

    // Mètode equals. Comprova si dos objectes Token són iguals
    public boolean equals(Object o) {//compara dos objectes token
        Token t = (Token) o;
        if (t.ttype == this.ttype){//si tenen el mateix ttype depen del ttype comparara
            if (t.ttype== Toktype.NUMBER){//si es un numero compara els valors i si son iguals retorna true
                if (t.value == this.value){
                    return true;
                }else return false;
            }else{
                if (t.tk == this.tk){//si no es un numero comparara els tk's i si son iguals retorna true
                    return true;
                }else return false;
            }
        }
        return false;
    }

    // A partir d'un String, torna una llista de tokens
    public static Token[] getTokens(String expr) {//metode que apartir de un string retorna un array de Tokens

        List<Token> lista = new ArrayList<>();//lista que convertirem en el array a retornar
        String reserva = "";//com els numeros de mes de un digit tenen cada digit .charAt's diferents,
        //utilitzam aquesta variable per a guardar el numero complet.

        for (int i = 0; i <expr.length() ; i++) {//bucle que analitza caracter per caracter
            if (expr.charAt(i) >= '0' && expr.charAt(i) <= '9' ){//si es un numero el guarda en una reserva
                // per tal de no tenir erros en casos de numeros de mes de dos digits
                reserva += expr.charAt(i);
            }else{//si no es un numero es un parentesis o un operador
                if (reserva.length()>0) {//si la reserva te un numero
                    lista.add(Token.tokNumber(parseInt(reserva)));//converteix el numero en un int,
                    // crida al constructor del token i l'afegeix a la llista
                    reserva = "";//buida la reserva
                }
                if (expr.charAt(i) == '(' || expr.charAt(i) == ')'){//si es un parentesis rida al constructor i l'afegeix a la llista
                    lista.add(Token.tokParen(expr.charAt(i)));

                }else {
                    lista.add(Token.tokOp(expr.charAt(i)));//si es un operador crida al constructor i l'afegeix a la llista
                }
            }
        }
        if (reserva.length() >0){//si la reserva segueix plena afegeix-la a la llista i buida la reserva.
            lista.add(Token.tokNumber(parseInt(reserva)));
            reserva = "";
        }
        //crea un array del tamany de la llista i copia la llista dins el array i retorna'l.
        Token[] tokens = new Token[lista.size()];
        tokens = lista.toArray(tokens);
        System.out.println(Arrays.toString(tokens));
        return tokens;
    }
}
