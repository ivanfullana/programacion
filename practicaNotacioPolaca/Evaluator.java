package practicaNotacioPolaca;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Evaluator {


    public static int calculate(String expr) {
        // Convertim l'string d'entrada en una llista de tokens
        Token[] tokens = Token.getTokens(expr);
        // Efectua el procediment per convertir la llista de tokens en notació RPN
        // Finalment, crida a calcRPN amb la nova llista de tokens i torna el resultat
        List<Token> output = new ArrayList<>();
        Stack<Token> operadors = new Stack<>();
        for (int i = 0; i < tokens.length; i++) {//bucle que mira token per token
            if (tokens[i].getTtype() == Token.Toktype.NUMBER) {//si es un numero afegeix-lo al output.
                output.add(tokens[i]);
            }
            if (tokens[i].getTtype() == Token.Toktype.OP) {//si es un operador aplicarem les preferències de shunting yard
                    if (tokens[i].getTk() == '+' || tokens[i].getTk() == '-') {//si es un operadornde preferencia 2 el que ficam..
                        if (operadors.isEmpty()) {//si el stack es buit fes push sense problemes
                            operadors.push(tokens[i]);
                        }else{
                            if (operadors.lastElement().getTk() == '+' || operadors.lastElement().getTk() == '-'){//si el darrer element es un operador de pref 2.
                                output.add(operadors.lastElement());//afegeix al outpu el darrer element
                                operadors.remove(operadors.lastElement());//elimina el darrer element
                                operadors.push(tokens[i]);//fes pus al stack del nou operador
                            }else{//si no es buida i no es de pref 2, ha de ser de pref 3 o major pertant
                                while (!operadors.isEmpty()){//fins que es buidi el stack
                                    output.add(operadors.lastElement());//afegeix al output el darrer operador
                                    operadors.remove(operadors.lastElement());//elimina el darrer operador
                                }
                                operadors.push(tokens[i]);//fes push del nou token.
                            }
                        }
                    }
                    if (tokens[i].getTk() == '*' || tokens[i].getTk() == '/'){//si el operador a afegir es de preferencia 3
                        if (operadors.lastElement().getTk() == '*' || operadors.lastElement().getTk() == '/'){// si el darrer operador es de preferència 3
                            output.add(operadors.lastElement());//tansols treu el darrer operador afegint-lo al output
                            operadors.remove(operadors.lastElement());//elimina el darrer operador del stack ja afegit
                            operadors.push(tokens[i]);//fes push del nou operador
                        }else{//si es buida, el operador es de preferència 2 o es un parentesis
                            operadors.push(tokens[i]);//fes push del operador senseproblemes
                        }
                    }
            }
        }
        while (!operadors.isEmpty()){//una vegada acabat el bucle buida el stack dels operadors restants
            output.add(operadors.lastElement());
            operadors.remove(operadors.lastElement());
        }

        tokens = new Token[output.size()];//cream l'array del tamany del output
        tokens = output.toArray(tokens);//convertim el output en array
        int resultat = calcRPN(tokens);//cream la variable resultat i cridam al calcRPN pasantli la nova notació.

        return resultat;
    }

    public static int calcRPN(Token[] list) {
        // Calcula el valor resultant d'avaluar la llista de tokens
        List<Token> Tokens = new ArrayList<>();//lista on es realitzaran les operacionsi guardaran numeros i resultats

        for (int i = 0; i <list.length ; i++) {//leggeix caracter per caracter per realitzar les operacions que toquin
            if (list[i].getTtype() == Token.Toktype.NUMBER){//si es un numero afegil a la llista
                Tokens.add(list[i]);
            }

            if (list[i].getTtype() == Token.Toktype.OP){//si es un operador
                if (list[i].getTk() == '*'){//en el cas de que sigui multiplicacio
                    //treu els dos darrers valors sense importar l'ordre i multiplicals
                    int resultat = Tokens.get(Tokens.size()-1).getValue() * Tokens.get(Tokens.size()-2).getValue();
                    //borra els numeros ja utilitzats
                    Tokens.remove(Tokens.size()-1);
                    Tokens.remove(Tokens.size()-1);
                    //amb el resultat de la operació crea un token i afegil ala llista
                    Tokens.add(Token.tokNumber(resultat));
                }
                if (list[i].getTk() == '/'){//en el cas de que haguem de dividir
                    //treu el penultim i despres l'ultimm numero, en aquest ordre, per tal de realitzar correctament l'operació
                    int resultat = Tokens.get(Tokens.size()-2).getValue() / Tokens.get(Tokens.size()-1).getValue();
                    //elimina els numeros utilitzats
                    Tokens.remove(Tokens.size()-1);
                    Tokens.remove(Tokens.size()-1);
                    Tokens.add(Token.tokNumber(resultat));//crea el token amb el constructor i afegil a la llista
                }
                if (list[i].getTk() == '-'){//en el cas de que sigui una resta
                    //treu el penultim i despres l'ultimm numero, en aquest ordre, per tal de realitzar correctament l'operació
                    int resultat = Tokens.get(Tokens.size()-2).getValue() - Tokens.get(Tokens.size()-1).getValue();
                    //elimina els numeros utilitzats
                    Tokens.remove(Tokens.size()-1);
                    Tokens.remove(Tokens.size()-1);
                    Tokens.add(Token.tokNumber(resultat));//crea el token amb el resultat pasant-lo al constructor i afegeixlo a la llista
                }
                if (list[i].getTk() == '+'){
                    //treu els dos darrers valors sense importar l'ordre i sumals
                    int resultat = Tokens.get(Tokens.size()-1).getValue() + Tokens.get(Tokens.size()-2).getValue();
                    //elimina els numeros utilitzats
                    Tokens.remove(Tokens.size()-1);
                    Tokens.remove(Tokens.size()-1);
                    Tokens.add(Token.tokNumber(resultat));//crea el token del resultat i afegeixlo a la llista
                }
            }
        }
        return Tokens.get(Tokens.size()-1).getValue(); //el darrer nvalor que quedi sera el resultat a retornar.
    }
}
